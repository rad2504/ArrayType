package com.typeofarray;
import java.util.Scanner;
public class arraytype {

    public static int typeofarray(int[] array,int n)
    {
        int odd=0,even=0;
        for(int i=0;i<n;i++)
        {
            if(array[i]%2==0)
            {
                odd++;
            }
            else
            {
                even++;
            }
        }
        if(odd==n)
        {
            return 1;
        }
        else if(even==n)
        {
            return 2;
        }
        else
        {
            return 3;
        }
    }
    public static void main(String[] args) {
	// write your code here
        Scanner scn=new Scanner(System.in);
        int n=scn.nextInt();
        int[] array=new int[n];
        for(int i=0;i<n;i++)
        {
            array[i]=scn.nextInt();
        }
        int type=typeofarray(array,n);
        if(type==1)
        {
            System.out.println("Even");
        }
        else if(type==2)
        {
            System.out.println("Odd");
        }
        else
        {
            System.out.println("Mixed");
        }
    }
}
